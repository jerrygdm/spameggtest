//
//  SpamEgg.swift
//  SpamEgg
//
//  Created by Gianmaria Dal Maistro on 20/12/16.
//  Copyright © 2016 com.whiteworld.test. All rights reserved.
//

import Foundation

enum Spammable
{
    case Spam
    case Egg
    case SpamEgg
    case Number(num: Int)
    
    var associatedValue: String {
        get {
            switch self {
            case    .Spam: return "Spam"
            case    .Egg: return "Egg"
            case    .SpamEgg: return "SpamEgg"
            case    .Number(let value): return String(value)
            }
        }
    }
}

struct SpamEgg
{
    var from : Int
    var to : Int
    
    func printSpamEgg()
    {
        for item in from...to
        {
            print(check(number: item))
        }
    }
    
    func check(number : Int) -> String
    {
        if (isDivisibleByThreeAndFive(number: number))
        {
            return Spammable.SpamEgg.associatedValue
        }
        else if (isDivisibleByThree(number: number))
        {
            return Spammable.Spam.associatedValue
        }
        else if (isDivisibleByFive(number: number))
        {
            return Spammable.Egg.associatedValue
        }
        
        return Spammable.Number(num: number).associatedValue
    }
    
    private func isDivisibleByThree(number : Int) -> Bool
    {
        return isDivisibleBy(divisor: 3, number: number)
    }
    
    private func isDivisibleByFive(number : Int) -> Bool
    {
        return isDivisibleBy(divisor: 5, number: number)
    }
    
    private func isDivisibleByThreeAndFive(number : Int) -> Bool
    {
        return isDivisibleBy(divisor: 3, number: number) && isDivisibleBy(divisor: 5, number: number)
    }
    
    private func isDivisibleBy(divisor: Int, number: Int) -> Bool
    {
        return number % divisor == 0
    }
}
