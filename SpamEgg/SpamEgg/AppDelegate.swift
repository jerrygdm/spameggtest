//
//  AppDelegate.swift
//  SpamEgg
//
//  Created by Gianmaria Dal Maistro on 20/12/16.
//  Copyright © 2016 com.whiteworld.test. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {

        // Second Exercise
        let spam = SpamEgg(from: 1, to: 100)
        spam.printSpamEgg()

        // Third Exercise
        let factCalc = 20
        print("Factorial of \(factCalc) is \(factorial(num: factCalc))")
        
        return true
    }
    
    func factorial(num : Int) -> Int
    {
        if num == 0
        {
            return 1
        }
        else
        {
            return num * factorial(num: num-1)
        }
    }
}
