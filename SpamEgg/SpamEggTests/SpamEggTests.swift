//
//  SpamEggTests.swift
//  SpamEggTests
//
//  Created by Gianmaria Dal Maistro on 20/12/16.
//  Copyright © 2016 com.whiteworld.test. All rights reserved.
//

import XCTest
@testable import SpamEgg

class SpamEggTests: XCTestCase
{
    var spamEgg : SpamEgg?
    
    override func setUp()
    {
        super.setUp()

        self.spamEgg = SpamEgg(from: 1, to: 99)
    }
    
    override func tearDown()
    {
        spamEgg = nil
    
        super.tearDown()
    }

    func testSpamEggNotNil()
    {
        XCTAssertNotNil(self.spamEgg)
    }

    func testPrintSpamIfInputThree()
    {
        XCTAssertEqual(spamEgg?.check(number: 3), "Spam")
    }

    func testPrintFourIfInputFour()
    {
        XCTAssertEqual(spamEgg?.check(number: 4), "4")
    }
    
    func testPrintEggIfInputFive()
    {
        XCTAssertEqual(spamEgg?.check(number: 5), "Egg")
    }
    
    func testPrintSpamEggIfInputFifteen()
    {
        XCTAssertEqual(spamEgg?.check(number: 15), "SpamEgg")
    }
}
